/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_socket_close)
{
	int32_t handle;
	int32_t open_r;
	int32_t close_r;

	tlog(T_INFO, "Test_habmm_socket_close begin\n");
	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_socket_close  the habmm_socket_open return value open_r=%d  handle=%d\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	/* Negative testing */
	/* 1.input invalidate handle */
	tlog(T_INFO, "1.Test_habmm_socket_close Negative testing :input invalid handle  \n");
	close_r = habmm_socket_close(112);
	tlog(T_INFO, "1.Test_habmm_socket_close Negative testing the return value close_r=%d  \n", close_r);
	EXPECT_INT_LT(close_r, 0);

	/* Positive testing */
	tlog(T_INFO, "Test_habmm_socket_close Positive testing:\n");
	close_r = habmm_socket_close(handle);
	tlog(T_INFO, "Test_habmm_socket_close Positive testing  the return value close_r=%d \n", close_r);
	EXPECT_INT_EQ(0, close_r);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}

static void add_tests(void)
{
	ADD_TEST(Test_habmm_socket_close);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test close initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test close  module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");