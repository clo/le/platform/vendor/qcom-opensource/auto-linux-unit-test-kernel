/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ion_kernel.h>
#include <linux/msm_ion.h>
#include <linux/export.h>
#include <linux/ktf.h>
#include <linux/dma-mapping.h>
#include <linux/dma-contiguous.h>
#include <linux/dma-buf.h>

KTF_INIT();

void *invalid_address=(void *) 0x0000000008000000;

TEST(hab_handler, Test_habmm_export)
{
	int32_t handle;
	int32_t open_r;
	int32_t export_r;
	uint32_t export_id;
	int32_t unexport_r;
	uint32_t export_id2;
	char *addr = NULL;
	struct dma_buf *vhandle = NULL;
	struct dma_buf *size_10 = NULL;
	int32_t bufsz = 4096;
	unsigned long err_ion_ptr = 0;

	vhandle = ion_alloc(bufsz, ION_HEAP(ION_SYSTEM_HEAP_ID), 0);
	if (IS_ERR_OR_NULL((void *)(vhandle))) {
		if (IS_ERR((void *)(vhandle)))
			err_ion_ptr = PTR_ERR((int *)(vhandle));
		tlog(T_INFO, " ION alloc fail err ptr=%ld\n",
		        err_ion_ptr);
		goto err;
	}

	size_10 = ion_alloc(10, ION_HEAP(ION_SYSTEM_HEAP_ID), 0);
	if (IS_ERR_OR_NULL((void *)(size_10))) {
		if (IS_ERR((void *)(size_10)))
			err_ion_ptr = PTR_ERR((int *)(size_10));
		tlog(T_INFO, " ION alloc fail err ptr=%ld\n",
		        err_ion_ptr);
		goto err;
	}

	dma_buf_begin_cpu_access((struct dma_buf*)vhandle,
				      DMA_BIDIRECTIONAL);
	addr = dma_buf_vmap((struct dma_buf*)vhandle);
	tlog(T_INFO, "Test_habmm_export begin: habmm_socket_open the return value addr->size=%d \n", addr);

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_export begin: habmm_socket_open the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	/* Positive testing */
	tlog(T_INFO, "Test_habmm_export Positive testing : size_bytes is 4096 and flag is 0x00020000\n");
	export_r = habmm_export(handle, vhandle ,4096, &export_id, HABMM_EXPIMP_FLAGS_DMABUF);
	export_id2 = export_id;
	tlog(T_INFO, "Test_habmm_export Positive testing size_bytes is 4096 and flag is 0x00020000 the return value export_r=%d export_id=%d \n", export_r, export_id);
	EXPECT_INT_EQ(0, export_r);

	/* unexport the space */
	unexport_r = habmm_unexport(handle, export_id2, 0x00000000);
	tlog(T_INFO, "unexport the space  flag is 0x00000000 the return value unexport_r=%d export_id2=%d \n", unexport_r, export_id2);
	EXPECT_INT_EQ(0, unexport_r);

	/* Negative testing */
	/* 1.handle is error */
	tlog(T_INFO, "1.habmm_export Negative testing : handle is error flag=0x00020000\n");
	export_r = habmm_export(11, vhandle, 4096, &export_id, HABMM_EXPIMP_FLAGS_DMABUF);
	tlog(T_INFO, "1.habmm_export Negative testing the return value export_r=%d  flag=0x00020000\n", export_r);
	EXPECT_INT_LT(export_r, 0);

	/* 2.buff_to_share is null or invalid address */
	tlog(T_INFO, "2.habmm_export Negative testing : buff_to_share is null  flag=0x00020000\n");
	export_r = habmm_export(handle, NULL, 4096, &export_id, HABMM_EXPIMP_FLAGS_DMABUF);
	tlog(T_INFO, "2.habmm_export Negative testing the return value export_r=%d flag=0x00020000 \n", export_r);
	EXPECT_INT_LT(export_r, 0);

	//tlog(T_INFO, "3.habmm_export Negative testing : buff_to_share is invalid address  flag=0x00000001\n");
	//export_r = habmm_export(handle, invalid_address, 4096, &export_id, 0x00000001);
	//tlog(T_INFO, "3.habmm_export Negative testing the return value export_r=%d flag=0x00000001 \n",export_r);
	//EXPECT_INT_LT(export_r, 0);

	//tlog(T_INFO, "3.habmm_export Negative testing : buff_to_share is invalid address  flag=0x00010000\n");
	//export_r = habmm_export(handle, invalid_address, 4096, &export_id, 0x00010000);
	//tlog(T_INFO, "3.habmm_export Negative testing the return value export_r=%d flag=0x00010000 \n",export_r);
	//EXPECT_INT_LT(export_r, 0);

	/*3.export_id is null */
	tlog(T_INFO, "3.habmm_export Negative testing : export_id is null    flag=0x00020000\n");
	export_r = habmm_export(handle, vhandle, 4096, NULL, HABMM_EXPIMP_FLAGS_DMABUF);
	tlog(T_INFO, "3.habmm_export Negative testing the return value export_r=%d flag=0x00020000 \n", export_r);
	EXPECT_INT_LT(export_r, 0);
	/* 4.Size is not page aligned */
	tlog(T_INFO, "4.habmm_export Negative testing : Size is not page aligned flag=0x00020000\n");
	export_r = habmm_export(handle, size_10, 10, &export_id, HABMM_EXPIMP_FLAGS_DMABUF);
	tlog(T_INFO, "4.habmm_export Negative testing the return value export_r=%d  flag=0x00020000\n", export_r);
	EXPECT_INT_LT(export_r, 0);

	/* free the buf */
	dma_buf_vunmap((struct dma_buf*)vhandle, addr);
	dma_buf_end_cpu_access((struct dma_buf*)vhandle, DMA_BIDIRECTIONAL);
	dma_buf_put(vhandle);
	dma_buf_put(size_10);

	habmm_socket_close(handle);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}


static void add_tests(void)
{
	ADD_TEST(Test_habmm_export);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test export initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test export module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");