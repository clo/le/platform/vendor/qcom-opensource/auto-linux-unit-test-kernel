/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/ktf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_socket_send)
{
	int32_t handle;
	int32_t open_r;
	int32_t send_r;
	char size_1= 'a';

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_socket_send begin: habmm_socket_open the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	/* #define HAB_HEADER_SIZE_MASK 0x0000FFFF the max size is 65535 */
	char* buff_p = kmalloc(65535, GFP_KERNEL);
	char* buff_n = kmalloc(65560, GFP_KERNEL);

	/* Positive testing */
	tlog(T_INFO, "Test_habmm_socket_send Positive testing : size_bytes is 1 and flag is 0\n");
	send_r = habmm_socket_send(handle, &size_1, 1, 0);
	tlog(T_INFO, "Test_habmm_socket_send Positive testing size_bytes is 1 and flag is 0 the return value send_r=%d  \n", send_r);
	EXPECT_INT_EQ(0, send_r);

	tlog(T_INFO, "Test_habmm_socket_send Positive testing : size_bytes is 65535 and flag is 0\n");
	send_r = habmm_socket_send(handle, buff_p, 65535, 0);
	tlog(T_INFO, "Test_habmm_socket_send Positive testing size_bytes is 65535 and flag is 0 the return value send_r=%d  \n", send_r);
	EXPECT_INT_EQ(0, send_r);

	tlog(T_INFO, "Test_habmm_socket_send Positive testing : size_bytes is 1 and flag is 1\n");
	send_r = habmm_socket_send(handle, &size_1, 1, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "Test_habmm_socket_send Positive testing size_bytes is 1 and flag is 1 the return value send_r=%d  \n", send_r);
	EXPECT_INT_EQ(0, send_r);

	tlog(T_INFO, "Test_habmm_socket_send Positive testing : size_bytes is 65535 and flag is 1\n");
	send_r = habmm_socket_send(handle, buff_p, 65535, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "Test_habmm_socket_send Positive testing size_bytes is 65535 and flag is 1 the return value send_r=%d  \n", send_r);
	EXPECT_INT_EQ(0, send_r);

	/* Negative testing */
	/* 1.get invalid handle */
	tlog(T_INFO, "1.Test_habmm_socket_send Negative testing begin: input invalid handle flag is 1\n");
	send_r = habmm_socket_send(1, &size_1, 1, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "1.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_INT_LT(send_r, 0);

	tlog(T_INFO, "1.Test_habmm_socket_send Negative testing begin: input invalid handle flag is 0\n");
	send_r = habmm_socket_send(1, &size_1, 1, 0);
	tlog(T_INFO, "1.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_INT_LT(send_r, 0);

	/* 2.input src_buff is null */
	tlog(T_INFO, "2.Test_habmm_socket_send Negative testing : input src_buff is null  flag is 1\n");
	send_r = habmm_socket_send(handle, NULL, 1, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "2.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_INT_LT(send_r, 0);

	tlog(T_INFO, "2.Test_habmm_socket_send Negative testing : input src_buff is null  flag is 0\n");
	send_r = habmm_socket_send(handle, NULL, 1, 0);
	tlog(T_INFO, "2.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_INT_LT(send_r, 0);
	/* 3.input size is size_bytes is 65560 out of size bound */
	tlog(T_INFO, "3.Test_habmm_socket_send Negative testing : input size_bytes is out of bound flag is 1\n");
	send_r = habmm_socket_send(handle, buff_n, 65560, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "3.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_INT_LT(send_r, 0);

	tlog(T_INFO, "3.Test_habmm_socket_send Negative testing : input size_bytes is out of bound flag is 0\n");
	send_r = habmm_socket_send(handle, buff_n, 65560, 0);
	tlog(T_INFO, "3.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_INT_LT(send_r, 0);

	habmm_socket_close(handle);
	kfree(buff_p);
	kfree(buff_n);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}



static void add_tests(void)
{
	ADD_TEST(Test_habmm_socket_send);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test send initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test send  module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");