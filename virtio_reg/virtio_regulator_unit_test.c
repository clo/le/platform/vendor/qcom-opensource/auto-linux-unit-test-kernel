/* Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/regulator/consumer.h>
#include <linux/regulator/driver.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/reset.h>
#include <linux/ktf.h>

KTF_INIT();

#define DRV_NAME	"virt_reg_test"
#define MAX_REG     13

static struct device *dev_ut;
static struct regulator *reg_ut;


typedef struct virtio_reg_ctx {
	const char *reg_consumer_id[MAX_REG];
	char *invalid_consumer_id[5];
}virtio_reg_ctx;

static struct virtio_reg_ctx reg_test_ctx = {
	.reg_consumer_id = {
		"unit_test_reg_1",
		"unit_test_reg_2",
		"unit_test_reg_3",
		"unit_test_reg_4",
		"unit_test_reg_5",
		"unit_test_reg_6",
		"unit_test_reg_7",
		"unit_test_reg_8",
		"unit_test_reg_9",
		"unit_test_reg_10",
		"unit_test_reg_11",
		"unit_test_reg_12",
		"unit_test_reg_13",
	},
	.invalid_consumer_id = { "test_cfg_ahb_reg", "\028", (char*) 0 },
};

TEST(regulator_handler, regulator_get_test)
{
	int i;

	/*Positive testing*/
	for (i = 0; i < MAX_REG; i++) {
		tlog(T_INFO, "Regulator name[%d] = %s\n", i, reg_test_ctx.reg_consumer_id[i]);
		reg_ut = regulator_get(dev_ut, reg_test_ctx.reg_consumer_id[i]);
		tlog(T_INFO, "Regulator Addr[%d] = 0x%x\n", i, reg_ut);
		ASSERT_OK_ADDR_GOTO(reg_ut, err);
		regulator_put(reg_ut);
	}

	/*Negative testing*/
	reg_ut = regulator_get(NULL, "unit_test_reg_1");
	tlog(T_INFO, "regulator_get with (NULL device *) return = %ld\n", PTR_ERR(reg_ut));
	EXPECT_INT_LT(PTR_ERR(reg_ut), 0);

	for (i = 0; i < 3; i++) {
		reg_ut = regulator_get(dev_ut, reg_test_ctx.invalid_consumer_id[i]);
		tlog(T_INFO, "regulator_get with invalid consumer id return = %ld\n", PTR_ERR(reg_ut));
		EXPECT_INT_LT(PTR_ERR(reg_ut), 0);
	}

	return;

err:
	terr("Can't get Regulator %s\n", reg_test_ctx.reg_consumer_id[i]);
}

TEST(regulator_handler, regulator_enable_test)
{
	int i;
	int ret = 0;

	/*Positive testing*/
	for (i = 0; i < MAX_REG; i++) {
		reg_ut = regulator_get(dev_ut, reg_test_ctx.reg_consumer_id[i]);
		if (!IS_ERR_OR_NULL(reg_ut)) {
			ret = regulator_enable(reg_ut);
			tlog(T_INFO, "regulator_enable[%s] return %d\n", reg_test_ctx.reg_consumer_id[i], ret);
			EXPECT_INT_EQ(0, ret);
			regulator_put(reg_ut);
		} else {
			tlog(T_INFO, "Regulator %s get failed\n", reg_test_ctx.reg_consumer_id[i]);
		}
	}
}

TEST(regulator_handler, regulator_disable_test)
{
	int i;
	int ret = 0;

	/*Positive testing*/
	for (i = 0; i < MAX_REG; i++) {
		reg_ut = regulator_get(dev_ut, reg_test_ctx.reg_consumer_id[i]);
		if (!IS_ERR_OR_NULL(reg_ut)) {
			ret = regulator_enable(reg_ut);
			EXPECT_INT_EQ(0, ret);
			ret = regulator_disable(reg_ut);
			tlog(T_INFO, "regulator_disable[%s] return %d\n", reg_test_ctx.reg_consumer_id[i], ret);
			EXPECT_INT_EQ(0, ret);
			regulator_put(reg_ut);
		} else {
			tlog(T_INFO, "Regulator %s get failed\n", reg_test_ctx.reg_consumer_id[i]);
		}
	}
}

TEST(regulator_handler, regulator_set_voltage_test)
{
	int ret = 0;

	/*Positive testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_12");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_set_voltage(reg_ut, 800000, 800000);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_set_voltage(reg_ut, 1000000, 1000000);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_set_voltage(reg_ut, 1400000, 1400000);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		/* only max_uV > rdev->constraints->max_uV or min_uV < rdev->constraints->min_uV are invalid */
		ret = regulator_set_voltage(reg_ut, 700000, 800000);
		EXPECT_INT_EQ(ret, 0);
		ret = regulator_set_voltage(reg_ut, 1400000, 1500000);
		EXPECT_INT_EQ(ret, 0);
		ret = regulator_set_voltage(reg_ut, 900000, 1000000);
		EXPECT_INT_EQ(ret, 0);
		ret = regulator_set_voltage(reg_ut, -1, 800000);
		EXPECT_INT_EQ(ret, 0);
		regulator_put(reg_ut);
	} else {
		tlog(T_INFO, "regulator unit_test_reg_12 get failed\n");
	}

	/*Negative testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_12");
	if (!IS_ERR_OR_NULL(reg_ut)) {
/* 		BUG_ON() if min_uV > max_uV, below cases will cause segmentation fault crash

		ret = regulator_set_voltage(reg_ut, 900000, 800000);
		ret = regulator_set_voltage(reg_ut, INT_MIN-1, 800000);
		ret = regulator_set_voltage(reg_ut, 800000, INT_MAX+1);
*/
		/* both min_uV and max_uV actually overlaps the constraints will cause error */
		ret = regulator_set_voltage(reg_ut, 700000, 700000);
		EXPECT_INT_LT(ret, 0);
		ret = regulator_set_voltage(reg_ut, 1500000, 1500000);
		EXPECT_INT_LT(ret, 0);
		ret = regulator_set_voltage(reg_ut, 500000, 700000);
		EXPECT_INT_LT(ret, 0);
		ret = regulator_set_voltage(reg_ut, 1500000, 1700000);
		EXPECT_INT_LT(ret, 0);
	} else {
		tlog(T_INFO, "unit_test_reg_12 get failed\n");
	}

	return;

err:
	tlog(T_INFO, "regulator unit_test_reg_12 set voltage failed\n");
	regulator_put(reg_ut);
}

TEST(regulator_handler, regulator_get_voltage_test)
{
	int vout;
	int ret = 0;

	/*Positive testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_12");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_set_voltage(reg_ut, 800000, 800000);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_enable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		vout = regulator_get_voltage(reg_ut);
		ASSERT_INT_GE_GOTO(vout, 800000, err);
		ret = regulator_set_voltage(reg_ut, 800000, 1400000);
		vout = regulator_get_voltage(reg_ut);
		ASSERT_TRUE_GOTO((vout >= 800000 && vout <= 1400000), err);
		ret = regulator_disable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_ut);
	} else {
		tlog(T_INFO, "regulator unit_test_reg_12 get failed\n");
	}

	return;

err:
	tlog(T_INFO, "regulator unit_test_reg_12 get voltage failed\n");
	regulator_put(reg_ut);
}

TEST(regulator_handler, regulator_set_mode_test)
{
	int ret = 0;

	/*Positive testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_1");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_enable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_mode(reg_ut, REGULATOR_MODE_FAST);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_set_mode(reg_ut, REGULATOR_MODE_NORMAL);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_set_mode(reg_ut, REGULATOR_MODE_IDLE);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_set_mode(reg_ut, REGULATOR_MODE_STANDBY);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_disable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_ut);
	} else {
		tlog(T_INFO, "regulator unit_test_reg_1 get failed\n");
	}

	/*Negative testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_1");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_enable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_mode(NULL, REGULATOR_MODE_NORMAL);
		EXPECT_INT_LT(ret, 0);
		ret = regulator_set_mode(reg_ut, 0x10);
		EXPECT_INT_LT(ret, 0);
		ret = regulator_set_mode(reg_ut, UINT_MAX+1);
		EXPECT_INT_LT(ret, 0);
	}

	return;

err:
	tlog(T_INFO, "regulator unit_test_reg_1 set mode failed\n");
	regulator_put(reg_ut);
}

TEST(regulator_handler, regulator_get_mode_test)
{
	unsigned int mode;
	int ret = 0;

	/*Positive testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_1");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_enable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_mode(reg_ut, REGULATOR_MODE_NORMAL);
		EXPECT_INT_EQ(0, ret);
		mode = regulator_get_mode(reg_ut);
		ASSERT_INT_EQ_GOTO(0x2, mode, err);
		ret = regulator_disable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_ut);
	} else {
		tlog(T_INFO, "regulator unit_test_reg_1 get failed\n");
	}

	/*Negative testing*/
	regulator_get_mode(NULL);
	EXPECT_INT_LT(ret, 0);

	return;

err:
	tlog(T_INFO, "regulator gdsc-pcie0 get mode failed\n");
	regulator_put(reg_ut);
}

TEST(regulator_handler, regulator_set_load_test)
{
	int ret = 0;

	/*Positive testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_10");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_enable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_load(reg_ut, 200000);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		ret = regulator_disable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_load(reg_ut, 0);
		ASSERT_INT_EQ_GOTO(0, ret, err);
		regulator_put(reg_ut);
	} else {
		tlog(T_INFO, "regulator unit_test_reg_10 get failed\n");
	}

	/*Negative testing*/
	reg_ut = regulator_get(dev_ut, "unit_test_reg_10");
	if (!IS_ERR_OR_NULL(reg_ut)) {
		ret = regulator_enable(reg_ut);
		EXPECT_INT_EQ(0, ret);

		/* For uA_load, any value is valid, expect return 0 */
		ret = regulator_set_load(reg_ut, -1);
		EXPECT_INT_EQ(ret, 0);
		ret = regulator_set_load(reg_ut, UINT_MAX+1);
		EXPECT_INT_EQ(ret, 0);
		ret = regulator_disable(reg_ut);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_ut);
	} else {
		tlog(T_INFO, "regulator unit_test_reg_10 get failed\n");
	}

	return;

err:
	tlog(T_INFO, "regulator unit_test_reg_10 set load failed\n");
	regulator_put(reg_ut);
}

static const struct of_device_id virt_reg_test_dt_ids[] = {
	{ .compatible = "qcom,virtio-regulator-unit-test" },
	{}
};

static int virt_reg_test_probe(struct platform_device *pdev)
{
	const struct of_device_id *id;
	tlog(T_INFO, "virt_reg_test_probe\n");
	id = of_match_device(virt_reg_test_dt_ids, &pdev->dev);
	if (!id) {
		terr("No matching device found\n");
		return -ENODEV;
	}
	tlog(T_INFO, "of_match_device, &pdev->dev = 0x%x\n", &pdev->dev);

	dev_ut = &pdev->dev;

	return 0;
}

static int virt_reg_test_remove(struct platform_device *pdev)
{
	return 0;
}

static void add_tests(void)
{
	ADD_TEST(regulator_get_test);
	ADD_TEST(regulator_enable_test);
	ADD_TEST(regulator_disable_test);
	ADD_TEST(regulator_set_voltage_test);
	ADD_TEST(regulator_get_voltage_test);
 /* virtio regulator doesn't support set mode currently, pending below 2 cases */
//	ADD_TEST(regulator_set_mode_test);
//	ADD_TEST(regulator_get_mode_test);
	ADD_TEST(regulator_set_load_test);
}

static struct platform_driver virt_reg_test_driver = {
	.probe = virt_reg_test_probe,
	.remove = virt_reg_test_remove,
	.driver = {
		.name = DRV_NAME,
		.of_match_table = virt_reg_test_dt_ids,
	},
};

static int __init virtio_reg_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for virtio regulator initialising\n");

	add_tests();

	ret = platform_driver_register(&virt_reg_test_driver);
	if (ret)
		terr("Error registering platform driver\n");

	tlog(T_INFO, "Driver initialized\n");
	return ret;
}

static void __exit virtio_reg_ut_exit(void)
{
	platform_driver_unregister(&virt_reg_test_driver);
	tlog(T_INFO, "virtio_regulator unit test kernel module Unloaded\n");
	KTF_CLEANUP();
}

module_init(virtio_reg_ut_init);
module_exit(virtio_reg_ut_exit);
MODULE_LICENSE("GPL v2");
